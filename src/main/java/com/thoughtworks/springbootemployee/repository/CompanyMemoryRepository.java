package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyMemoryRepository {

    @Autowired
    EmployeeMemoryRepository employees;

    public List<Company> companies = new ArrayList<>();

    private static AtomicLong atomicId = new AtomicLong(0);

    public Company createCompany(Company company) {
        company.setId(atomicId.incrementAndGet());
        companies.add(company);
        return company;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyById(Long id) {
        return companies.stream().filter(company -> company.getId().equals(id)).findFirst().get();
    }

    public List<Employee> getEmployeesByCompanyId(Long id) {
        return employees.getEmployeeByCompanyId(id);
    }


    public List<Company> queryCompanies(int page, int pageSize) {
        return companies.stream()
                .skip((long) (page - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company updateCompany(Long id, Company company) {
        return companies.stream().filter(companyItem -> companyItem.getId().equals(id)).findFirst().map(item -> {
            item.setName(company.getName());
            return item;
        }).get();
    }

    public void deleteCompany(Long id) {
        Company companyItem = companies.stream().filter(company -> company.getId().equals(id)).findFirst().get();
        companies.remove(companyItem);
    }
}
