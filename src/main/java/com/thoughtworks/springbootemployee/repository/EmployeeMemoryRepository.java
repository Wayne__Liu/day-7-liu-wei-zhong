package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeMemoryRepository {

    public List<Employee> employees = new ArrayList<>();
    private static AtomicLong atomicId = new AtomicLong(0);

    public Employee createEmployee( Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().get();
    }

    public List<Employee> getEmployeeByGender(String gender) {
        return employees.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());
    }

    public Employee updateEmployee(Long id, Employee employee) {
        return employees.stream().filter(employeeItem -> employeeItem.getId().equals(id)).findFirst().map(item -> {
            item.setAge(employee.getAge());
            item.setSalary(employee.getSalary());
            return item;
        }).get();
    }

    public void deleteEmployee(Long id){
        Employee employee1 = employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().get();
        employees.remove(employee1);
    }

    public List<Employee> queryEmployee(int page,  int pageSize){
        int startIndex = (page - 1) * pageSize;
        return employees.stream()
                .skip(startIndex)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeeByCompanyId(Long id) {
        return employees.stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
    }
}
